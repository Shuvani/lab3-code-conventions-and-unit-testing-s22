package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.UncategorizedSQLException;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class threadControllerTest {

  private static final Integer THREAD_ID = 545;
  private static final Integer THREAD_ID2 = 5;
  private static final String THREAD_SLUG = "slugId";
  private static final String USER_NICKNAME = "nickname";

  ThreadController threadController = new ThreadController();
  Thread threadWithId;
  Thread threadWithSlug;
  Post post;
  List<Post> postList;
  User user;
  Vote vote;

  @BeforeEach
  void setUp() {
    threadWithId = new Thread(
      "Anna",
      Timestamp.from(Instant.now()),
      "forum",
      "message",
      "slug",
      "title",
      2
    );
    threadWithId.setId(THREAD_ID);

    threadWithSlug = new Thread(
      "Lesha",
      Timestamp.from(Instant.now()),
      "forum",
      "message",
      THREAD_SLUG,
      "title",
      1
    );
    threadWithSlug.setId(THREAD_ID2);

    post = new Post(
      USER_NICKNAME,
      Timestamp.from(Instant.now()),
      null,
      "message",
      1,
      THREAD_ID,
      false
    );

    postList = new ArrayList<>();
    postList.add(post);

    user = new User(
      USER_NICKNAME,
      "email",
      "fullname",
      "about"
    );

    vote = new Vote(
      USER_NICKNAME,
      1
    );
  }

  @Test
  void testCheckIdOrSlug_WhenPassedInteger_ReturnObject() {
    try (MockedStatic<ThreadDAO> threadButNotThread = Mockito.mockStatic(ThreadDAO.class)) {
      threadButNotThread.when(() -> ThreadDAO.getThreadById(THREAD_ID)).thenReturn(threadWithId);

      assertThat(threadController.CheckIdOrSlug(THREAD_ID.toString()))
        .isEqualTo(threadWithId);
    }
  }

  @Test
  void testCheckIdOrSlug_WhenPassedSlug_ReturnObject() {
    try (MockedStatic<ThreadDAO> threadButNotThread = Mockito.mockStatic(ThreadDAO.class)) {
      threadButNotThread.when(() -> ThreadDAO.getThreadBySlug(THREAD_SLUG)).thenReturn(threadWithSlug);

      assertThat(threadController.CheckIdOrSlug(THREAD_SLUG))
        .isEqualTo(threadWithSlug);
    }
  }

  @Test
  void createPost_WhenDbFalls_ReturnNotFound() {
    try (MockedStatic<ThreadDAO> threadButNotThread = Mockito.mockStatic(ThreadDAO.class)) {
      threadButNotThread.when(() -> ThreadDAO.getThreadBySlug(Mockito.anyString()))
                        .thenThrow(new UncategorizedSQLException("", "", new SQLException()));


      assertThat(threadController.createPost(THREAD_SLUG, postList))
        .isEqualTo(ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Раздел не найден.")));
    }
  }

  @Test
  void createPost_WhenThreadIsFound_ReturnCreated() {
    try (MockedStatic<ThreadDAO> threadButNotThread = Mockito.mockStatic(ThreadDAO.class)) {
      try (MockedStatic<UserDAO> userButNotUser = Mockito.mockStatic(UserDAO.class)) {
        threadButNotThread.when(() -> ThreadDAO.getThreadBySlug(THREAD_SLUG)).thenReturn(threadWithSlug);
        userButNotUser.when(() -> UserDAO.Info(USER_NICKNAME)).thenReturn(user);

        assertThat(threadController.createPost(THREAD_SLUG, postList))
          .isEqualTo(ResponseEntity.status(HttpStatus.CREATED).body(postList));
      }
    }
  }

  @Test
  void Posts_WhenDbFalls_ReturnNotFound() {
    try (MockedStatic<ThreadDAO> threadButNotThread = Mockito.mockStatic(ThreadDAO.class)) {
      threadButNotThread.when(() -> ThreadDAO.getThreadBySlug(Mockito.anyString()))
                        .thenThrow(new UncategorizedSQLException("", "", new SQLException()));


      assertThat(threadController.Posts(THREAD_SLUG, 1, 0, "sort", false))
        .isEqualTo(ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Раздел не найден.")));
    }
  }

  @Test
  void Posts_WhenThreadIsFound_ReturnCreated() {
    try (MockedStatic<ThreadDAO> threadButNotThread = Mockito.mockStatic(ThreadDAO.class)) {
      threadButNotThread.when(() -> ThreadDAO.getThreadBySlug(THREAD_SLUG)).thenReturn(threadWithSlug);
      threadButNotThread.when(() -> ThreadDAO.getPosts(threadWithSlug.getId(), 1, 0, "sort", false))
                        .thenReturn(postList);


      assertThat(threadController.Posts(THREAD_SLUG, 1, 0, "sort", false))
        .isEqualTo(ResponseEntity.status(HttpStatus.OK).body(postList));
    }
  }

  @Test
  void change_WhenEverythingIsOk_ReturnCreated() {
    try (MockedStatic<ThreadDAO> threadButNotThread = Mockito.mockStatic(ThreadDAO.class)) {
      threadButNotThread.when(() -> ThreadDAO.getThreadBySlug(THREAD_SLUG)).thenReturn(threadWithSlug);
      threadButNotThread.when(() -> ThreadDAO.getThreadById(THREAD_ID2)).thenReturn(threadWithSlug);

      assertThat(threadController.change(THREAD_SLUG, threadWithId))
        .isEqualTo(ResponseEntity.status(HttpStatus.OK).body(threadWithSlug));
    }
  }

  @Test
  void info_WhenEverythingIsOk_ReturnCreated() {
    try (MockedStatic<ThreadDAO> threadButNotThread = Mockito.mockStatic(ThreadDAO.class)) {
      threadButNotThread.when(() -> ThreadDAO.getThreadBySlug(THREAD_SLUG)).thenReturn(threadWithSlug);

      assertThat(threadController.info(THREAD_SLUG))
        .isEqualTo(ResponseEntity.status(HttpStatus.OK).body(threadWithSlug));
    }
  }

  @Test
  void createVote_WhenEverythingIsOk_ReturnCreated() {
    try (MockedStatic<ThreadDAO> threadButNotThread = Mockito.mockStatic(ThreadDAO.class)) {
      try (MockedStatic<UserDAO> userButNotUser = Mockito.mockStatic(UserDAO.class)) {
        threadButNotThread.when(() -> ThreadDAO.getThreadBySlug(THREAD_SLUG)).thenReturn(threadWithSlug);
        userButNotUser.when(() -> UserDAO.Info(USER_NICKNAME)).thenReturn(user);

        assertThat(threadController.createVote(THREAD_SLUG, vote))
          .isEqualTo(ResponseEntity.status(HttpStatus.OK).body(threadWithSlug));
      }
    }
  }
}
